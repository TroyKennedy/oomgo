package oomgo

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type Movie struct {
	Title    string `json:"Title"`
	Year     string `json:"Year"`
	Rated    string `json:"Rated"`
	Released string `json:"Released"`
	Runtime  string `json:"Runtime"`
	Genre    string `json:"Genre"`
	Director string `json:"Director"`
	Writer   string `json:"Writer"`
	Actors   string `json:"Actors"`
	Plot     string `json:"Plot"`
	Language string `json:"Language"`
	Country  string `json:"Country"`
	Awards   string `json:"Awards"`
	Poster   string `json:"Poster"`
	Ratings  []struct {
		Source string `json:"Source"`
		Value  string `json:"Value"`
	} `json:"Ratings"`
	Metascore  string `json:"Metascore"`
	ImdbRating string `json:"imdbRating"`
	ImdbVotes  string `json:"imdbVotes"`
	ImdbID     string `json:"imdbID"`
	Type       string `json:"Type"`
	DVD        string `json:"DVD"`
	BoxOffice  string `json:"BoxOffice"`
	Production string `json:"Production"`
	Website    string `json:"Website"`
	Response   string `json:"Response"`
}

func GetMovie(title string, apikey string) (omdbresp Movie) {
	resp, err := http.Get(fmt.Sprintf("http://www.omdbapi.com/?t=%s&apikey=%s", title, apikey))
	if err != nil {
		println("No response")
	}
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	var movieresp Movie
	json.Unmarshal(body, &movieresp)
	return movieresp
}
